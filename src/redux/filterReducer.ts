import products from "../redux/data/level3/products.json";
import {ALL_BRANDS, FILTER_BRAND} from "./actionTypes";
import {FilterReducer, Product, ProductState} from "../react-app-env";

const allProducts: any = products;

const initialState: ProductState = {
  products: allProducts,
  brand: 0
}

const filterReducer: FilterReducer = (state = initialState, action) => {

  switch (action.type) {
    case FILTER_BRAND:
      return {
        products: allProducts.filter((obj: Product) => obj.brand === action.payload),
        brand: action.payload
      };
    case ALL_BRANDS:
      return initialState;

    default:
      return state;
  }
}

export default filterReducer;
