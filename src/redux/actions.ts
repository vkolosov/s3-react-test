import {FILTER_BRAND, ALL_BRANDS, ADD_PRODUCT, REMOVE_PRODUCT, CHANGE_COUNT_PRODUCT} from "./actionTypes";
import {BasketReducerAction, customizationProductOptions, FilterReducerAction, Product} from "../react-app-env";

export function createFilter(brand: number): FilterReducerAction {
  if (brand === 0) {
    return {
      type: ALL_BRANDS,
      payload: 0
    }
  }

  return {
    type: FILTER_BRAND,
    payload: brand
  }
}

export function removeProductBasket(product: Product): BasketReducerAction {
  return {
    type: REMOVE_PRODUCT,
    payload: {product}
  }
}

export function addProductBasket(product: Product, customizationProductOptions: customizationProductOptions): BasketReducerAction {
  return {
    type: ADD_PRODUCT,
    payload: {product, customizationProductOptions}
  }
}

export function changeCountBasket(product: Product, newCount: number): BasketReducerAction {
  return {
    type: CHANGE_COUNT_PRODUCT,
    payload: {
      product,
      newCount
    }
  }
}