import {combineReducers, createStore} from "redux";
import filterReducer from "./filterReducer";
import basketReducer from "./basketReducer";

const rootReducer = combineReducers({
  filter: filterReducer,
  basket: basketReducer
})

const store = createStore(rootReducer);

export default store;