import {BasketReducer, BasketReducerAction, BasketState} from "../react-app-env";
import {ADD_PRODUCT, CHANGE_COUNT_PRODUCT, REMOVE_PRODUCT} from "./actionTypes";

const initialState: BasketState = {
  products: null,
  multipleProducts: [],
  productsOptions: []
}

const basketReducer: BasketReducer = (basketState = initialState, action: BasketReducerAction) => {
  switch (action.type) {
    case ADD_PRODUCT:

      // если добавляется первый товар в корзину
      if (basketState.products === null) {
        return {
          products: [action.payload.product],
          multipleProducts: [{productId: action.payload.product.id, countProducts: 1}],
          productsOptions: action.payload.customizationProductOptions != null
              ? [action.payload.customizationProductOptions]
              : []
        };
      }

      const alreadyInBasket = basketState.products.find((item) => {
        return item.id === action.payload.product.id;
      });

      // если такого товара в корзине нет
      if (!alreadyInBasket) {

        return {
          products: basketState.products.concat(action.payload.product),
          multipleProducts: [...basketState.multipleProducts, {productId: action.payload.product.id, countProducts: 1}],
          productsOptions: action.payload.customizationProductOptions != null
              ? [...basketState.productsOptions, action.payload.customizationProductOptions]
              : basketState.productsOptions
        };
      }

      // если товар уже в корзине, ничего не делаем
      return basketState;

    case REMOVE_PRODUCT:
      if (basketState.products !== null) {
        const productIndex = basketState.products.findIndex((product) => product.id === action.payload.product.id);
        const multipleProductIndex = basketState.multipleProducts.findIndex((obj) => obj.productId === action.payload.product.id);
        const productOptionsIndex = basketState.productsOptions.findIndex((obj) => obj.productData.productId === action.payload.product.id);

        return {
          products: [...basketState.products.slice(0, productIndex), ...basketState.products.slice(+productIndex + 1)],

          multipleProducts: multipleProductIndex
              ? [...basketState.multipleProducts.slice(0, multipleProductIndex), ...basketState.multipleProducts.slice(+multipleProductIndex + 1)]
              : basketState.multipleProducts,

          productsOptions: productOptionsIndex
              ? [...basketState.productsOptions.slice(0, productOptionsIndex), ...basketState.productsOptions.slice(+productOptionsIndex + 1)]
              : basketState.productsOptions
        };
      }
      return basketState;


    case CHANGE_COUNT_PRODUCT:
      return {
        products: basketState.products,
        multipleProducts: basketState.multipleProducts.map(item => {
          if (
              item.productId === action.payload.product.id &&
              action.payload.newCount !== undefined
          ) {
            return {productId: item.productId, countProducts: action.payload.newCount};
          }
          return item;
        }),
        productsOptions: basketState.productsOptions
      };

    default:
      return basketState;
  }
}

export default basketReducer;
