/// <reference types="react-scripts" />

import {Reducer} from "redux";
import {ReducerAction} from "react";

export declare type Product = {
  type: string;
  id: number;
  sku: string;
  title: string;
  regular_price: {
    currency: string;
    value: number;
  };
  image: string;
  configurable_options?: [
    {
      attribute_id: number;
      attribute_code: string;
      label: string;
      position?: number;
      id?: number;
      values: [
        {
          label: string;
          value_index: number;
          value: number;
        }
      ];
    }
  ];
  variants?: [
    {
      attributes: [
        {
          code: string;
          value_index: number;
        }
      ];
      product: {
        id: number;
        sku: string;
        image: string;
      }
    }
  ];
  brand: number;
}

export declare type Brand = {
  id: number;
  title: string;
  sort: string;
  code: string;
}

export declare type ProductState = {
  readonly products: Product[];
  readonly brand: number
}

export declare type BasketState = {
  products: Product[] | null;
  multipleProducts: { productId: number, countProducts: number }[] | [];
  productsOptions: customizationProductOptions[] | [];
}

export declare type customizationProductOptions = {
  productData: {
    productId: number;
    size: number;
    color: number;
  }
}

export declare interface FilterReducer extends Reducer {
  (state: ProductState, action: FilterReducerAction): ProductState
}

export declare interface BasketReducer {
  (state: BasketState, action: BasketReducerAction): BasketState
}

export declare interface FilterReducerAction extends ReducerAction {
  type: string;
  payload: number;
}

export declare type BasketReducerAction = {
  type: string;
  payload: {
    product: Product;
    newCount?: number;
    customizationProductOptions?: customizationProductOptions;
  };
}
