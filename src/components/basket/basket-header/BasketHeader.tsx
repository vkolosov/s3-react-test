import './BasketHeader.css';
import {Link} from "react-router-dom";

const BasketHeader = () => {
  return (
      <header className="basket-header">
        <nav className="basket-header__nav">
          <span> </span>
          <Link to="/">
            <img className="basket-header__logo"
                 src="/assets/logo.png" alt=""/>
          </Link>
          <Link className="basket-header__to-catalog-link" to="/">В каталог</Link>
        </nav>
      </header>
  );
};

export default BasketHeader;