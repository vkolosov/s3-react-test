import './BasketItem.css';
import {customizationProductOptions, Product} from "../../../react-app-env";
import React from "react";
import {useDispatch} from "react-redux";
import {changeCountBasket, removeProductBasket} from "../../../redux/actions";

const BasketItem = ({
                      product,
                      qty,
                      options
                    }: {
                      product: Product,
                      qty: { productId: number, countProducts: number } | undefined,
                      options?: customizationProductOptions
                    }
) => {

  const dispatch = useDispatch();

  // собрал вручную массив соотношений опций и их идов
  const colorIndexes: any = {
    Red: 931,
    Blue: 932,
    Black: 933
  }

  const sizeIndexes: any = {
    M: 1441,
    L: 1442
  };

  const deleteProduct = () => {
    dispatch(removeProductBasket(product));
  }

  const changeCountProduct = (e: React.BaseSyntheticEvent) => {
    if (e.target.value < 1) {
      dispatch(removeProductBasket(product));
    }
    dispatch(changeCountBasket(product, e.target.value));
  }

  const getProductColor = (): string | undefined => {
    if (options == null || options.productData.color === 0 || product.type === 'simple') {
      return undefined;
    }
    for (let key in colorIndexes) {
      if (+options.productData.color === +colorIndexes[key]) {
        return `Color: ${key}`;
      }
    }
  }

  const getProductSize = () => {
    if (options == null || options.productData.size === 0 || product.type === 'simple') {
      return undefined;
    }
    for (let key in sizeIndexes) {
      if (+options.productData.size === +sizeIndexes[key]) {
        return `Size: ${key}`;
      }
    }
  }

  return (
      <div className="basket-item d-flex justify-content-between mx-sm-3">
        <img className="basket-item__image mx-sm-2" width="75" height="75"
             src={`/assets${product.image.replace(/\/image\//, '/images/')}`} alt=""/>
        <span className="basket-item__title flex-grow-1 mx-sm-2">
            <span>{product.title}</span><br/>
            <span>{getProductSize()}</span><br/>
            <span>{getProductColor()}</span><br/>
        </span>
        <span className="basket-item__price mx-sm-2">${product.regular_price.value}</span>
        <span className="basket-item__qty ms-1 mx-sm-2">
          <input
              onChange={changeCountProduct}
              className="basket-item__qty-input"
              type="number"
              value={qty?.countProducts || 0}
          />

        </span>
        <span
            className="basket-item__total mx-sm-2">{(product.regular_price.value * (qty?.countProducts || 0)).toFixed(2)}</span>
        <i onClick={deleteProduct} className="bi bi-trash-fill"> </i>
      </div>
  );
};

export default BasketItem;