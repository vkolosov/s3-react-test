import './BasketContent.css';
import BasketItem from "../BasketItem/BasketItem";
import {useSelector} from "react-redux";
import {BasketState} from "../../../react-app-env";

const BasketContent = () => {

  let basketState = useSelector(({basket}: { basket: BasketState }): BasketState => basket);

  const getTotalSum = () => {
    return basketState.products?.reduce((prev, cur) => {

      const count = basketState.multipleProducts.find(
          item => item.productId === cur.id
      );

      if (count == null) {
        return 0;
      }

      return (
          cur.regular_price.value * count.countProducts + prev
      );
    }, 0).toFixed(2);
  }

  return (
      <div className="basket-content">
        <h2 className="mx-3">Shopping Cart</h2>
        <div className="basket-content__header d-flex justify-content-between mx-sm-3">
          <div className="basket-content__div-block mx-sm-2"/>
          <span className="basket-content__title flex-grow-1 mx-sm-2">title</span>
          <span className="basket-content__price mx-sm-2">value</span>
          <span className="basket-content__qty ms-1 mx-sm-2">Qty</span>
          <span className="basket-content__total mx-sm-2">Total</span>
          <span className="basket-content__div-block-2"> </span>
        </div>
        {
          basketState.products !== null &&
          basketState.products.map(
              (basketProduct) =>
                  <BasketItem
                      key={basketProduct.id}
                      product={basketProduct}
                      qty={basketState.multipleProducts.find(
                          (item) =>
                              item.productId === basketProduct.id
                      )}
                      options={basketState.productsOptions.find(item => item.productData.productId === basketProduct.id)}
                  />
          )
        }
        <div className="basket-content__subtotal d-flex flex-column align-items-end">
          <span>Subtotal: {getTotalSum()}</span>
          <button type="button" className="basket-content__checkout btn btn-success">Checkout</button>
        </div>

      </div>

  );
};

export default BasketContent;