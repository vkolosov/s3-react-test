import ProductItem from "../product-item/ProductItem";
import './Catalog.css';
import {useSelector} from "react-redux";
import {Product, ProductState} from "../../react-app-env";


const Catalog = () => {

  const filterState = useSelector(({filter}: { filter: ProductState }): ProductState => filter);

  const productsList: any = filterState.products.map(
      (product: Product) => <ProductItem key={product.id} product={product}/>
  );

  return (
      <div className="catalog">
        <h2>Catalog</h2>
        <div className="catalog__items">
          {productsList.length !== 0 ? productsList : 'Таких товаров нет =('}
        </div>
      </div>
  );
};

export default Catalog;
