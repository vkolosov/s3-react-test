import './ProductItem.css';
import {BasketState, Product} from "../../react-app-env";
import {useDispatch, useSelector} from "react-redux";
import {addProductBasket} from "../../redux/actions";
import React, {useState} from "react";

const ProductItem = ({product}: { product: Product }) => {

  const dispatch = useDispatch();
  let basketState = useSelector(({basket}: { basket: BasketState }): BasketState => basket);

  // показывать надпись над товаром при наведении или нет
  const [isShow, setIsShow] = useState(false);
  // выбранный размер товара, либо 0
  const [selectedSize, setSelectedSize] = useState(0);
  // выбранный цвет товара, либо 0
  const [selectedColor, setSelectedColor] = useState(0);

  const getProductImageLink = () => {
    if (selectedColor !== 0 && selectedSize !== 0) {
      const configurationData = getConfigurationData(product, selectedColor, selectedSize);
      if (configurationData != null) {
        // поправил битую ссылку из жсона
        return `/assets${configurationData.image.replace(/\/image\//, '/images/')}`;
      }
    }
    return `/assets${product.image}`;
  }

  const isInBasket = (id: number) => {
    return basketState.products?.find(item => item.id === id);
  }

  // генерируем имя класса для надписи над товаром при наведении
  const createClassName = (): string => {
    return `product-item__add-to-basket-desc ${isShow && !isInBasket(product.id) ? 'active' : ''}`;
  }

  // возвращает объект опций выбранного кастомного товара
  const getConfigurationData = (product: Product, color: number, size: number) => {
    if (product != null && product.variants != null) {
      const customProduct = product.variants.find(item => {
        if (
            item.attributes.find(item2 => item2.value_index === size) &&
            item.attributes.find(item3 => item3.value_index === color)
        ) {
          return true;
        }
        return false;
      });
      if (customProduct != null) {
        return {
          configurationId: customProduct?.product.id,
          sku: customProduct.product.sku,
          image: customProduct.product.image
        }
      }
      return null;
    }
  }

  const addToBasket = (e: React.SyntheticEvent) => {
    if (getAddToBasketTitle().includes('Добавить в корзину')) {
      if (!e.currentTarget.className.includes('in-basket')) {
        const configurationData = getConfigurationData(product, selectedColor, selectedSize);
        if (
            product.type.includes('configurable') &&
            configurationData != null
        ) {
          // если продукт кастомный, перед отправкой в стейт корзины меняем его ску и картинку на кастомные
          product = {...product, ...configurationData};
        }

        dispatch(addProductBasket(
                product,
                {productData: {productId: product.id, size: selectedSize, color: selectedColor}}
            )
        );
      }
    }
  }

  const onMouseProduct = () => {
    setIsShow(true)
  }

  const onMouseLeave = () => {
    setIsShow(false)
  }

  const selectSize = (e: React.BaseSyntheticEvent) => {
    // если пользователь нажал на ту же опцию, переключаем ее
    if (selectedSize === +e.currentTarget.getAttribute('data-value')) {
      setSelectedSize(0);
    } else if (!e.currentTarget.className.includes('out-of-stock')) {
      setSelectedSize(+e.currentTarget.getAttribute('data-value'))
    }
  }

  const selectColor = (e: React.BaseSyntheticEvent) => {
    // если пользователь нажал на ту же опцию, переключаем ее
    if (selectedColor === +e.currentTarget.getAttribute('data-value')) {
      setSelectedColor(0);
    } else if (!e.currentTarget.className.includes('out-of-stock')) {
      setSelectedColor(+e.currentTarget.getAttribute('data-value'))
    }
  }

  // создаем надпись над товаром при наведении
  const getAddToBasketTitle = () => {
    if (
        product.type === 'configurable' &&
        (selectedSize === 0 ||
            selectedColor === 0)
    ) {
      return 'Выберите опции';
    }
    return 'Добавить в корзину'
  }

  // проверяем, есть ли товар с заданными опциями
  const checkOptionStock = (size: number, color: number) => {
    const variantAttributes = product.variants?.map(item => item.attributes);

    if (variantAttributes != null) {
      return variantAttributes.find(item => {
        const stockSize = item.find(item2 => {
          return size === item2.value_index
        })

        const stockColor = item.find(item2 => {
          return color === item2.value_index
        })

        return stockColor && stockSize;
      });
    }
    return undefined;
  }

  // отрисовываем значок цвета опции
  const colorOptionsHandler = () => {
    const productOptions = product.configurable_options?.find(item => item.label === 'Color');
    if (productOptions != null) {
      return productOptions.values.map(confOptionValues => {
        let inStock: object | undefined = {inStock: 'inStock'};
        if (selectedSize !== 0) {
          inStock = checkOptionStock(selectedSize, confOptionValues.value_index);
        }

        const className = () => {
          // если ничего не выбрано, отрисовываем все
          if (selectedColor === 0 && selectedSize === 0) {
            return 'product-item__option-color';
            // если цвет выбран, и он отличный от этого элемента, выключаем текущий
          } else if (selectedColor !== 0 && selectedColor !== confOptionValues.value_index) {
            return 'product-item__option-color out-of-stock';
          }
          return `product-item__option-color ${inStock ? 'in-stock' : 'out-of-stock'} ${selectedColor === confOptionValues.value_index ? 'active' : ''}`
        }

        return (
            <div
                onClick={selectColor}
                className={className()}
                style={{backgroundColor: String(confOptionValues.value)}}
                data-value={confOptionValues.value_index}
            >
            </div>
        )
      });
    }
    return undefined;
  }

  // отрисовываем значок размера опции
  const sizeOptionsHandler = () => {
    const productOptions = product.configurable_options?.find(item => item.label === 'Size');
    if (productOptions != null) {
      return productOptions.values.map(confOptionValues => {
        let inStock: object | undefined = {inStock: 'inStock'};
        if (selectedColor !== 0) {
          inStock = checkOptionStock(confOptionValues.value_index, selectedColor);
        }

        const className = () => {
          // если ничего не выбрано, отрисовываем все
          if (selectedColor === 0 && selectedSize === 0) {
            return 'product-item__option-size';
            // если размер выбран и он отличен текущего элемента, выключаем текущий
          } else if (selectedSize !== 0 && selectedSize !== confOptionValues.value_index) {
            return 'product-item__option-size out-of-stock';
          }
          return `product-item__option-size ${inStock ? 'in-stock' : 'out-of-stock'} ${selectedSize === confOptionValues.value_index ? 'active' : ''}`
        }

        return (
            <div
                onClick={selectSize}
                className={className()}
                data-value={confOptionValues.value_index}
            >
              {confOptionValues.label}
            </div>
        )
      })
    }
    return undefined;
  }

  return (
      <div
          onMouseEnter={onMouseProduct}
          onMouseLeave={onMouseLeave}
          className={`product-item flex-grow-0 ${isInBasket(product.id) ? 'in-basket' : ''}`}
      >
        <img src={getProductImageLink()} alt="product"/>
        <p onClick={addToBasket} className={createClassName()}>{getAddToBasketTitle()}</p>
        <p className="product-item__product-name">{product.title}</p>
        <p className="product-item__brand-name">{product.brand}</p>
        <p className="product-item__product-price">
          ${product.regular_price.value.toFixed(2)}
        </p>
        <div className="product-item__options d-flex flex-wrap justify-start">
          {colorOptionsHandler()}
          <div className="product-item__options-break"/>
          {sizeOptionsHandler()}
        </div>
      </div>
  );
};

export default ProductItem;