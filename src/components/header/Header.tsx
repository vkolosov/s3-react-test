import './Header.css';
import {useSelector} from "react-redux";
import {BasketState, Product} from "../../react-app-env";
import {Link} from "react-router-dom";

const Header = () => {

  const basketProducts = useSelector(({basket}: { basket: BasketState }): Product[] | null => basket.products);
  const productsCounter = basketProducts !== null ? basketProducts.length : 0;

  const IconElement = ({counter}: { counter: number }) => {

    if (counter === 0) {
      return null;
    }

    if (counter > 9) {

      return (
          <img
              className="header__products-counter"
              src="https://img.icons8.com/ios/50/000000/dots-loading.png"
              alt=""
          />
      )
    }

    return (
        <img
            className="header__products-counter"
            src={`https://img.icons8.com/color/48/000000/${productsCounter}-cute.png`}
            alt=""
        />
    )
  }

  return (
      <header className="header">
        <nav className="header__nav">

          <img className="header__logo"
               src="/assets/logo.png" alt=""/>

          <Link to="/basket" className="header__basket">
            <i className="bi bi-cart-check"/>
          </Link>

          <IconElement counter={productsCounter}/>

        </nav>
      </header>
  );
};

export default Header;