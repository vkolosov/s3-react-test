import './BrandFilter.css';
import allBrands from '../../redux/data/brands.json';
import BrandFilterItem from "../brand-filter-item/BrandFilterItem";
import {useDispatch, useSelector} from "react-redux";
import {createFilter} from "../../redux/actions";
import {Brand, ProductState} from "../../react-app-env";

const BrandFilter = () => {
  const dispatch = useDispatch();
  const activeBrand = useSelector(({filter}: { filter: ProductState }) => filter.brand);


  const brands: Brand[] = allBrands;

  const showAllProducts = () => {
    dispatch(createFilter(0));
  }

  return (
      <div className="brand-filter">
        <span
            className={`brand-filter__all-brands ${activeBrand === 0 ? 'active' : ''}`}
            onClick={showAllProducts}
        >
          All Brands
        </span>
        <ul className="d-flex flex-wrap flex-sm-column">
          {brands.map(
              (brand: Brand) =>
                  <li className="mx-1" key={brand.id}>
                    <BrandFilterItem brandData={brand}/>
                  </li>
          )}
        </ul>
      </div>
  );
};

export default BrandFilter;
