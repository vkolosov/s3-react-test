import './App.css';
import {Route, Routes} from "react-router-dom";
import CatalogPage from "../../pages/catalog-page/CatalogPage";
import BasketPage from "../../pages/basket-page/BasketPage";

function App() {

  return (
      <div className="app">
        <Routes>
          <Route path="/" element={<CatalogPage/>}/>
          <Route path="/basket" element={<BasketPage/>}/>
        </Routes>
      </div>
  );
}

export default App;
