import './BrandFilterItem.css';
import {useDispatch, useSelector} from "react-redux";
import {createFilter} from "../../redux/actions";
import {Brand, ProductState} from "../../react-app-env";

const BrandFilterItem = ({brandData}: { brandData: Brand }) => {
  const dispatch = useDispatch();
  // выбранный на данный момент фильтр по брендам
  const activeBrand = useSelector(({filter}: { filter: ProductState }) => filter.brand);
  const filterItems = (id: number) => {
    dispatch(createFilter(id));
  }

  const className = `brand-filter-item ${activeBrand === brandData.id ? 'active' : ''}`;

  return (
      <span onClick={() => filterItems(brandData.id)} className={className}>
        {brandData.title}
      </span>
  );
};

export default BrandFilterItem;