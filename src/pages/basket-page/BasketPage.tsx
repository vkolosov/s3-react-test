import './BasketPage.css';
import BasketHeader from "../../components/basket/basket-header/BasketHeader";
import Basket from "../../components/basket/Basket";
import Footer from "../../components/footer/Footer";

const BasketPage = () => {
  return (
      <div className="basket-page container-fluid">
        <div className="row gy-0">
          <div className="col-12 px-0 pb-3">
            <BasketHeader/>
          </div>
        </div>
        <div className="basket-page__content row">
          <div className="px-0 col-12">
            <Basket/>
          </div>
        </div>
        <div className="row">
          <div className="col-12 px-0 py-0 pt-3">
            <Footer/>
          </div>
        </div>
      </div>
  );
};

export default BasketPage;