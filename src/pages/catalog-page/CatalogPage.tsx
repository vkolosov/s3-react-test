import './CatalogPage.css';
import Header from "../../components/header/Header";
import BrandFilter from "../../components/brand-filter/BrandFilter";
import Catalog from "../../components/catalog/Catalog";
import Footer from "../../components/footer/Footer";

const CatalogPage = () => {
  return (
      <div className="catalog-page container-fluid">
        <div className="row gy-0">
          <div className="col-12 px-0 pb-3">
            <Header/>
          </div>
        </div>
        <div className="catalog-page__content row">
          <div className="item col-12 col-sm-3 col-lg-2 col-xl-1">
            <BrandFilter/>
          </div>
          <div className="col-12 col-sm-9 col-lg-10 col-xl-11">
            <Catalog/>
          </div>
        </div>
        <div className="row">
          <div className="col-12 px-0 py-0 pt-3">
            <Footer/>
          </div>
        </div>
      </div>
  );
};

export default CatalogPage;